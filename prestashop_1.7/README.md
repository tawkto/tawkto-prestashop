# Tawk.to Live Chat

Free live chat widget for your site

## Description

tawk.to is a  free  live chat app that lets you monitor and  chat  with  visitors on your website  or from a free customizable page. No catch. No spam. No wares. It's truly free and always will be.

No account ? [Create one for free](https://www.tawk.to/?utm_source=prestashop&utm_medium=link&utm_campaign=signup)

# Installation

This section describes how to install the plugin and get it working.

1. Download and extract the tawkto1.7.zip
2. Go to Dashboard -> Modules -> Modules and Services.
3. Click on the Upload a Module button
4. Drag and drop the tawkto1.7.zip, or upload the tawkto1.7.zip using the Upload a Module popup
5. Click on `Configure` to start setting your tawk.to module

https://www.tawk.to/?utm_source=prestashop&utm_medium=link&utm_campaign=signup

## Frequently Asked Questions

[FAQ](http://tawk.uservoice.com/knowledgebase)

