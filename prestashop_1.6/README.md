# Tawk.to Live Chat

Free live chat widget for your site

## Description

tawk.to is a  free  live chat app that lets you monitor and  chat  with  visitors on your website  or from a free customizable page. No catch. No spam. No wares. It's truly free and always will be.

No account ? [Create one for free](https://www.tawk.to/?utm_source=prestashop&utm_medium=link&utm_campaign=signup)

# Installation

This section describes how to install the plugin and get it working.

1. Download and extract the tawkto.zip
2. Upload `tawkto` directory to the `/modules/` directory
3. Install module from `Modules -> Front Office Features`
4. Go to `Administration`, then to `Tawk.to` to select page and widget you wish to use

## Frequently Asked Questions

[FAQ](http://tawk.uservoice.com/knowledgebase)
